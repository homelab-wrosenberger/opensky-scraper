
# Simple scraper for OpenSky

This repo contains a simple, quick-and-dirty scraper for gathering data from [OpenSky](https://opensky-network.org/).

## Development setup

1. Install everything with `make install`
    - This sets up a Python virtual environment for this project next to the `Makefile`
1. Run an example call with `make run`

## Production setup

1. Install with `make install`
1. Add a cron job to periodically run the ingest job.
    - Edit your crontabs with `crontab -e`
    - To run the job every 15 minutes, add the following:
        ```
        */15 * * * * /usr/bin/make -C /path/to/project/ run
        ```

## Usage

This package defines the following commands:
- `opensky_get`: Gather data from OpenSky and save it to disk.
    - Accepts a single command line argument containing the path to a directory where the data should
        be stored. If this argument is not set, the path defaults to the `dev_output` directory in
        this project.
    - When this command is executed, we query OpenSky for their most recent state data. These data are
        converted into a single CSV file and saved to the output directory. Included in the name of this
        CSV file is a POSIX timestamp describing the time when the data were downloaded.
