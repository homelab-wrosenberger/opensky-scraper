
install:
	python3 -m venv env && \
	. env/bin/activate && \
		pip install -r requirements.txt && \
		pip install -e src/


freeze:
	. env/bin/activate && \
		pip freeze --exclude-editable > requirements.txt

run:
	. env/bin/activate && \
		opensky_get
