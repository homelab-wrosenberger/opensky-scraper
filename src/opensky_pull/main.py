
import json
from urllib.parse import urljoin
import datetime
import time
import os
import argparse

import requests
import pandas as pd


# Get the path to where this project is installed.
# We use this to set the default output directory.
PROJ_DIR = os.path.abspath(
    os.path.join(
        os.path.realpath(__file__),
        "..",
        "..",
        ".."
    )
)
# Default location for storing the data.
DEFAULT_OUT = os.path.join(PROJ_DIR, 'dev_output')


OSKY_SITE = "https://opensky-network.org/"
OSKY_FLIGHTS = urljoin(OSKY_SITE, '/api/flights/all/')
OSKY_STATES = urljoin(OSKY_SITE, '/api/states/all')


def get_states():
    """
        Returns a Python JSON object containing the current results of OpenSky's
        states API.
    """
    r = requests.get(
        OSKY_STATES,
    )
    r.raise_for_status()

    return json.loads(r.content)


def states_to_df(json_data):
    """
        Parses the states API output and stores it as a Pandas dataframe
    """
    df = pd.DataFrame(
        json_data['states'],
        columns=[
            # Source of these column names can be found [here](https://opensky-network.org/apidoc/rest.html)
            'icao24',
            'callsign',
            'origin_country',
            'time_position',
            'last_contact',
            'longitude',
            'latitude',
            'baro_altitude',
            'on_ground',
            'velocity',
            'true_track',
            'vertical_rate',
            'sensors',
            'geo_altitude',
            'squawk',
            'spi',
            'position_source',
        ]
    )

    return df


def save_osky_states(df, date_of_get, out_path):
    """
        Save the data.
        
        `df` is the dataframe that should be saved.
        `date_of_get` is a datetime instance containing when these data were downloaded.
        `out_path` is the path to the directory where the output files should be stored.
    """
    
    df.to_csv(os.path.join(
        out_path,
        'osky_state_{timestamp}.csv'.format(
            timestamp=round(date_of_get.replace(tzinfo=datetime.timezone.utc).timestamp())
        )
    ))


def get_args():
    """
        Read in command line arguments 
    """
    parser = argparse.ArgumentParser(description="Download OpenSky's current data.")
    parser.add_argument(
        "out",
        type=str,
        nargs="?",
        help="Path to the directory where the data should be saved."
    )
    args = parser.parse_args()
    
    return args


def main():
    args = get_args()
    out_path = args.out if args.out else DEFAULT_OUT
    print("Output path is: {}".format(out_path))

    now = datetime.datetime.now()
    json_data = get_states()
    df = states_to_df(json_data)
    print(df)
    save_osky_states(df, now, out_path)


if __name__ == "__main__":
    main()
