
from setuptools import setup, find_packages

setup(
    name="opensky_pull",
    version="0.0.1",
    description="A simple script for pulling data from OpenSky",
    packages=find_packages(),
    install_requires=[
        'pandas',
        'requests'
    ],
    # Entrypoint scripts that should be runnable
    # from the command line.
    entry_points={
        "console_scripts": [
            "opensky_get=opensky_pull.main:main"
        ]
    }
)
